# Model View Controller Login (MVC-Login-2021)

Description: This is a combination of two Udemy courses about building an MVC Framework.
                It will be the basis of some "How To" videos to help others.

## Commits

* Initial commit - repo and project created
* Create the folder structure and added all the required files and classes
* Added Signup controller and view
    * Adds User to database and shows the success page
* Added server-side validation
* Added client-side validation using JavaScript and AJAX
* Server-side and Client-side validation completed
* Created controllers to signup and login/logout users
    * Implemented PHP sessions
* Begin restricting access to logged-in users
* Created a test controller to test implementation of restricting access
* Remember the page requested when requiring authentication
* Added method to require login on individual action methods in the Core\Controller
* Use the Before action filter to restrict access for all action methods in a controller
* Use an authenticating controller to restrict access on protected controllers
    * This implementation simplifies user access control 
* Get current authenticated user and refactored code to remove redundancy
* Begin adding Flash Notifications
* Completed adding Flash Notification with temporary css styles
* Begin adding methods to allow user to stay logged-in
* Added the checkbox to the login form and configured login controller to remember checked state
* Added set cookie (Remember the login in a cookie)
* Login automatically using the token in the cookie
* Prevent automatic login if token has expired
* Completed remembering user if they prefer to stay logged-in
    * As part of going to a production environment, this will have an alert the warns of the dangers of the "Keep me signed in"
* Install required packages for sending emails
    * Will attempt to use Mailgun for sending but have PHPMailer as backup
* Begin the password reset process part one
* Completed part one sending email with a link to reset password
* Begin part two reset password - process the link in the email
* Added form to reset password and methods to process the request
* Completed part two of the reset password
  * Tested and verified
* Account activation process completed and tested
* Added ability for the user to update their profile
  * Preparing to refactor code
* Refactor and User Profile functionality completed
* Rebuilt computer due to failed update
* Cloned to build a tutorial site
