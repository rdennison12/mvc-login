<?php
/**
 * Created by Rick Dennison
 * Date:      1/31/21
 *
 * File Name: RememberedLogin.php
 * Project:   MVC-Login-2021
 */

namespace App\Models;


use App\Token;
use Core\Model;
use Exception;
use PDO;
use function get_called_class;
use function strtotime;
use function time;

class RememberedLogin extends Model
{
    /**
     * @param $token
     * @return mixed
     * @throws Exception
     */
    public static function findByToken($token)
    {
        $token = new Token($token);
        $token_hash = $token->getHash();

        $sql = 'SELECT `token_hash`, `user_id`, `expires_at` FROM remembered_logins
                WHERE token_hash = :token_hash';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':token_hash', $token_hash, PDO::PARAM_STR);

        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();
        return $stmt->fetch();
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return User::findByID($this->user_id);
    }

    /**
     * @return bool
     */
    public function hasExpired(): bool
    {
        return strtotime($this->expires_at) < time();
    }

    public function delete()
    {
        $sql = 'DELETE FROM remembered_logins WHERE token_hash = :token_hash';
        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':token_hash', $this->token_hash, PDO::PARAM_STR);
        $stmt->execute();
    }
}