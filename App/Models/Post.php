<?php
/**
 * Created by Rick Dennison
 * Date:      1/28/21
 *
 * File Name: Post.php
 * Project:   MVC-Login-2021
 */

namespace App\Models;


use Core\Model;
use PDO;
use PDOException;

class Post extends Model
{
    /**
     * @return mixed
     */
    public static function getAll()
    {
        try {
            $db = static::getDB();

            $stmt = $db->query('SELECT id, title, content FROM posts
                                ORDER BY created_at');

            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}