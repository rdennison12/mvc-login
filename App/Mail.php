<?php
/**
 * Created by Rick Dennison
 * Date:      1/31/21
 *
 * File Name: Mail.php
 * Project:   MVC-Login-2021
 */

namespace App;

use App\Config;
use Mailgun\Mailgun;

class Mail
{
    /**
     * @param string $to Recipient
     * @param string $subject Subject
     * @param string $text Text-only content of the message
     * @param string $html HTML content of the message
     */
    public static function send(string $to, string $subject, string $text, string $html)
    {
        $mailgun = Mailgun::create(Config::MAILGUN_API_KEY, Config::MAILGUN_ENDPOINT);
        $domain = Config::MAILGUN_DOMAIN;

        $mailgun->messages()->send($domain, [
            'from' => 'rdennison12@gmail.com',
            'to' => $to,
            'subject' => $subject,
            'text' => $text,
            'html' => $html
        ]);
    }
}