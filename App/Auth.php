<?php
/**
 * Created by Rick Dennison
 * Date:      1/30/21
 *
 * File Name: Auth.php
 * Project:   MVC-Login-2021
 */

namespace App;


use App\Models\RememberedLogin;
use App\Models\User;
use Exception;
use function ini_get;
use function session_destroy;
use function session_get_cookie_params;
use function session_name;
use function session_regenerate_id;
use function setcookie;
use function time;


class Auth
{
    /**
     * @param User $user The User Model
     * @param $remember_me
     * @return void
     */
    public static function login(User $user, $remember_me)
    {
        session_regenerate_id(true);
        $_SESSION['user_id'] = $user->id;

        if ($remember_me) {
            if ($user->rememberLogin()) {
                setcookie('remember_me', $user->remember_token, $user->expiry_timestamp, '/');
            }
        }
    }

    /**
     * Logs the user out and destroys the SESSION
     */
    public static function logout()
    {
        // Unset all of the session variables.
        $_SESSION = array();

        // If it's desired to kill the session, also delete the session cookie.
        // Note: This will destroy the session, and not just the session data!
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"],
                $params["domain"],
                $params["secure"],
                $params["httponly"]
            );
        }

        // Finally, destroy the session.
        session_destroy();

        static::forgetLogin();
    }

    /**
     * Remember the originally-requested page in the session
     *
     * @return void
     */
    public static function rememberRequestedPage()
    {
        $_SESSION['return_to'] = $_SERVER['REQUEST_URI'];
    }

    /**
     * Gets the originally-requested page to return to
     * after requiring login, or default to the homepage
     */
    public static function getReturnToPage()
    {
        return $_SESSION['return_to'] ?? '/';
    }

    /**
     * Gets the current logged-in user
     * Either from the session or the remember-me cookie
     *
     * @return mixed
     */
    public static function getUser()
    {
        if (isset($_SESSION['user_id'])) {
            return User::findByID($_SESSION['user_id']);
        } else {
            return static::loginFromRememberCookie();
        }
    }

    /**
     * @return mixed
     * @throws Exception
     */
    protected static function loginFromRememberCookie()
    {
        $cookie = $_COOKIE['remember_me'] ?? false;
        if ($cookie) {
            $remembered_login = RememberedLogin::findByToken($cookie);
            if ($remembered_login && !$remembered_login->hasExpired()) {
                $user = $remembered_login->getUser();
                static::login($user, false);
                return $user;
            }
        }
    }

    public static function forgetLogin()
    {
        $cookie = $_COOKIE['remember_me'] ?? false;
        if ($cookie) {
            $remembered_login = RememberedLogin::findByToken($cookie);
            if ($remembered_login) {
                $remembered_login->delete();
            }
            // Sets expiry_timestamp into the past
            setcookie('remember_me', '', time() - 3600);
        }
    }
}