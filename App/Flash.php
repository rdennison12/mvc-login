<?php
/**
 * Created by Rick Dennison
 * Date:      1/30/21
 *
 * File Name: Flash.php
 * Project:   MVC-Login-2021
 */

namespace App;


class Flash
{
    /**
     * Success message type
     * @var string
     */
    const SUCCESS = 'success';

    /**
     * Information message type
     * @var string
     */
    const INFO = 'info';

    /**
     * Warning message type
     * @var string
     */
    const WARNING = 'warning';

    /**
     * @param string $message Message content
     * @param string $type The optional message type, defaults to SUCCESS
     */
    public static function addMessage(string $message, string $type = 'success')
    {
        // Create an array in the SESSION if it doesn't exist
        if (!isset($_SESSION['flash_notifications'])) {
            $_SESSION['flash_notifications'] = [];
        }
        // Appends the message to the array
        $_SESSION['flash_notifications'][] = [
            'body' => $message,
            'type' => $type];
    }

    public static function getMessages()
    {
        if (isset($_SESSION['flash_notifications'])) {
            $messages = $_SESSION['flash_notifications'];
            unset($_SESSION['flash_notifications']);
            return $messages;
        }
    }
}