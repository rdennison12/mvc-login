<?php
/**
 * Created by Rick Dennison
 * Date:      1/31/21
 *
 * File Name: Token.php
 * Project:   MVC-Login-2021
 */

namespace App;


use Exception;
use function bin2hex;
use function hash_hmac;
use function random_bytes;

class Token
{
    protected string $token;

    /**
     * Token constructor.
     * Creates a new random token.
     * @param null $token_value
     * @throws Exception
     */
    public function __construct($token_value = null)
    {
        if ($token_value) {
            $this->token = $token_value;
        } else {
            $this->token = bin2hex(random_bytes(16));
        }
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->token;
    }

    /**
     * @return string
     */
    public function getHash(): string
    {
        return hash_hmac('sha256', $this->token, Config::SECRET_KEY);
    }
}