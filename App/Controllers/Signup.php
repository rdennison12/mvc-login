<?php
/**
 * Created by Rick Dennison
 * Date:      1/27/21
 *
 * File Name: Signup.php
 * Project:   MVC-Login-2021
 */

namespace App\Controllers;


use App\Models\User;
use Core\Controller;
use Core\View;
use Exception;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use function header;
use function var_dump;

class Signup extends Controller
{
    /**
     * Show the Signup form
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function newAction()
    {
        View::renderTemplate('Signup/new.html');
    }

    /**
     * Create a new user in DB
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function createAction()
    {
        $user = new User($_POST);

        if ($user->save()) {
            $user->sendActivationEmail();
            $this->redirect('/Signup/success');
        } else {
            View::renderTemplate('Signup/new.html', [
                'user' => $user
            ]);
        }
    }

    /**
     * Show the success message page
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function successAction()
    {
        View::renderTemplate('Signup/success.html');
    }

    /**
     * Process a valid activation code
     * @throws Exception
     */
    public function activateAction()
    {
        User::activate($this->route_params['token']);
        $this->redirect('/Signup/activated');
    }

    /**
     * Show account activation success message and link to login
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function activatedAction()
    {
        View::renderTemplate('Signup/activated.html');
    }
}