<?php
/**
 * Created by Rick Dennison
 * Date:      1/30/21
 *
 * File Name: Items.php
 * Project:   MVC-Login-2021
 */

namespace App\Controllers;

use Core\View;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class Items extends Authenticated
{


    /**
     * Items index
     * @return void
     * @throws RuntimeError
     * @throws SyntaxError
     *
     * @throws LoaderError
     */
    public function indexAction()
    {
        View::renderTemplate('Items/index.html');
    }
}