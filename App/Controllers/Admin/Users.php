<?php
/**
 * Created by Rick Dennison
 * Date:      1/27/21
 *
 * File Name: Users.php
 * Project:   MVC-Login-2021
 */

namespace App\Controllers\Admin;


class Users
{
    /**
     * Before filter
     *
     * @return void
     */
    protected function before()
    {
        // Make sure an admin user is logged in for example
        // return false;
    }

    /**
     * Show the index page
     *
     * @return void
     */
    public function indexAction()
    {
        echo 'User admin index';
    }
}