<?php
/**
 * Created by Rick Dennison
 * Date:      1/30/21
 *
 * File Name: Authenticated.php
 * Project:   MVC-Login-2021
 */

namespace App\Controllers;


use Core\Controller;

abstract class Authenticated extends Controller
{
    /**
     * When a controller (class) uses this class it will implement
     * the before action filter and requires a user to authenticate
     * before gaining access to any methods in a controller
     */
    protected function before()
    {
        $this->requireLogin();
    }
}