<?php
/**
 * Created by Rick Dennison
 * Date:      1/28/21
 *
 * File Name: Account.php
 * Project:   MVC-Login-2021
 */

namespace App\Controllers;


use App\Models\User;
use Core\Controller;
use function header;
use function json_encode;

class Account extends Controller
{
    /**
     * Validate if the email is in use when user tries to signup
     * This uses an AJAX request for validation
     *
     * @return void
     */
    public function validateEmailAction()
    {
        $is_valid = !User::emailExists($_GET['email'], $_GET['ignore_id'] ?? null);

        header('Content-Type: application/json');
        echo json_encode($is_valid);
    }
}