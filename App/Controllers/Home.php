<?php
/**
 * Created by Rick Dennison
 * Date:      1/27/21
 *
 * File Name: Home.php
 * Project:   MVC-Login-2021
 */

namespace App\Controllers;


use App\Auth;
use Core\Controller;
use Core\View;

class Home extends Controller
{
    /**
     * Show the index page
     *
     * @return void
     */
    public function indexAction()
    {
        View::renderTemplate('Home/index.html');
    }
}