<?php
/**
 * Created by Rick Dennison
 * Date:      1/28/21
 *
 * File Name: Login.php
 * Project:   MVC-Login-2021
 */

namespace App\Controllers;


use App\Auth;
use App\Flash;
use Core\Controller;
use App\Models\User;
use Core\View;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class Login extends Controller
{
    /**
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function loginAction()
    {
        View::renderTemplate('Login/login.html');
    }

    /**
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function authenticateAction()
    {
        $user = User::authenticate($_POST['email'], $_POST['password']);
        $remember_me = isset($_POST['remember_me']);
        if ($user) {
            Auth::login($user, $remember_me);

            Flash::addMessage('Login successful');
            $this->redirect(Auth::getReturnToPage());
        } else {
            Flash::addMessage('Login failed, please try again.', Flash::WARNING);
            View::renderTemplate('Login/login.html', [
                'email' => $_POST['email'],
                'remember_me' => $remember_me
            ]);
            // TODO If user does not exist in DB show signup page
        }
    }

    /**
     * Log the user out
     * @return void
     */
    public function destroyAction()
    {
        Auth::logout();
        $this->redirect('/Login/show-logout-message');
    }

    /**
     * Show a "logged out" flash message and redirect to the homepage. Necessary to use the flash messages
     * as they use the session and at the end of the logout method (destroyAction) the session is destroyed
     * so a new action needs to be called in order to use the session.
     */
    public function showLogoutMessageAction(): void
    {
        Flash::addMessage('Logout successful');
        $this->redirect('/');
    }
}