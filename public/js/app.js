$.validator.addMethod('validPassword',
    function (value, element, param) {
        if (value !== '') {
            if (value.match(/.*[a-z]+.*/i) == null) {
                return false;
            } // TODO Add requirement for special characters (e.g. ! @ # $ % _) to add another layer of security
            if (value.match(/.*\d+.*/) == null) {
                return false;
            }
        }
        return true;
    },
    'Must contain at least one letter and one number'
);

/**
 * Show button to toggle password
 */
$('#inputPassword').hideShowPassword({
    show: false,
    innerToggle: 'focus'
});