<?php
/**
 * Created by Rick Dennison
 * Date:      1/27/21
 *
 * File Name: View.php
 * Project:   MVC-Login-2021
 */

namespace Core;


use App\Auth;
use App\Flash;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class View
{
    /**
     * Render a view template using Twig
     * @param string $template The template file
     * @param array $args Associative array of data to display in the view (optional)
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public static function renderTemplate(string $template, $args = [])
    {
        echo static::getTemplate($template, $args);
    }
    /**
     * Get the contents of a view template using Twig
     * @param string $template The template file
     * @param array $args Associative array of data to display in the view (optional)
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public static function getTemplate(string $template, $args = []): string
    {
        static $twig = null;

        if ($twig === null) {
            $loader = new \Twig\Loader\FilesystemLoader(dirname(__DIR__) . '/App/Views');
            $twig = new \Twig\Environment($loader);
            $twig->addGlobal('current_user', Auth::getUser());
            $twig->addGlobal('flash_messages', Flash::getMessages());
        }
        return $twig->render($template, $args);
    }
}
